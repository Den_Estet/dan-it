// let const блочная область видимости
// let можно переназначать переменую
// const нельзя менять переменую
// var можно переназначать переменую,
// переменная объявленная через var, видна везде
// Переменная объявлена через var будет глобальной.
// Обычно считается плохой практикой объявлять переменные глобальными.



const name = prompt("Введите ваше имя", ["Денис"]);
const age = +prompt("Введите ваш возраст", [22]);

if (age < 18) {
    alert("You are not allowed to visit this website")
} else if (age <= 22) {
    if (confirm("Are you sure you want to continue?"))
        alert(`Welcome, ${name}`)
    else alert("You are not allowed to visit this website")
} else {
    alert(`Welcome, ${name}`)
}
